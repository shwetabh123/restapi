﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restapi
{


    [TestClass]
   public  class RestAPITest
    {


        //POST REQUEST
        [TestMethod]
        public void CreateUsers()

        {

            string jsonString=@"{
                             ""name"": ""morpheus"",
                                 ""job"": ""leader""
           }";

            RestApiHelper<CreateUser> restapi = new RestApiHelper<CreateUser>();

            //resource url
            var restUrl = restapi.SetUrl("api/users");

            //POST
            var restRequest = restapi.CreatePostRequest(jsonString);


            var response = restapi.GetResponse(restUrl,restRequest);



            //retuns as object 
            CreateUser content = restapi.GetContent<CreateUser>(response);

            Assert.AreEqual(content.name, "morpheus");


            Console.WriteLine(content);

            Assert.AreEqual(content.job, "leader");






        }

        //not working

        //GET REQUEST
        [TestMethod]
        public void GetAPIresponseusinggivenendpoint()

        {


            RestApiHelper<CreateUser> restapi = new RestApiHelper<CreateUser>();

            //resource url
            var restUrl1 = restapi.SetUrl1("userinformation");



            //GET
            var restRequest = restapi.CreateGetRequest();


            var response = restapi.GetResponse(restUrl1, restRequest);



            //retuns as object 
            CreateUser content = restapi.GetContent<CreateUser>(response);

            Assert.AreEqual(content.name, "morpheus");


            Console.WriteLine(content);

            Assert.AreEqual(content.job, "leader");






        }

    }
}
