﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restapi
{

    //REST API testing using C RestSharp and Json.NET-
    public class RestApiHelper<T>
    {



        public RestClient _restClient;

        public RestRequest _restRequest;

        public string _baseUrl = "https://reqres.in/";

        public string _baseUrl1 = "http://mydomain.com/";



        public RestClient SetUrl(string resourceUrl)

        {
            var url = Path.Combine(_baseUrl, resourceUrl);

            var _restClient = new RestClient(url);

            return _restClient;

        }

        public  RestClient SetUrl1(string endpoint)

        {
            var url = Path.Combine(_baseUrl1, endpoint);

            var restClient = new RestClient(url);

            return restClient;

        }


        public RestRequest CreatePostRequest(string jsonString)
        {

            _restRequest = new RestRequest(Method.POST);

            _restRequest.AddHeader("Accept","application/json");


            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);


            return _restRequest;



        }


        public RestRequest CreatePutRequest(string jsonString)
        {

            _restRequest = new RestRequest(Method.PUT);

            _restRequest.AddHeader("Accept", "application/json");


            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);


            return _restRequest;



        }



        public RestRequest CreateGetRequest()
        {


            _restRequest = new RestRequest(Method.GET);
            _restRequest.AddHeader("Accept", "application/json");

            return _restRequest;


        }

        public RestRequest CreateDeleteRequest()
        {


            _restRequest = new RestRequest(Method.DELETE);
            _restRequest.AddHeader("Accept", "application/json");

            return _restRequest;


        }


        



        public IRestResponse GetResponse(RestClient restClient, RestRequest restRequest)

        {

            return restClient.Execute(restRequest);//RETURN AS STRING ..NEED TO DESERIALIZE STRINGS     AS OBJECTS


        }



        //RETURN AS STRING ..NEED TO DESERIALIZE STRINGS AS OBJECTS
        public DTO GetContent<DTO>(IRestResponse response)

        {
            var content = response.Content;

            DTO deserializeObject = JsonConvert.DeserializeObject<DTO>(content);

            return deserializeObject;
        }








    }
}
